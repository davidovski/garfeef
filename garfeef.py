#!/usr/bin/env python3
"""
A test bot using the Python Matrix Bot API
Test it out by adding it to a group chat and doing one of the following:
1. Say "Hi"
2. Say !echo this is a test!
3. Say !d6 to get a random size-sided die roll result
"""

import random
import urllib.request
import time

from matrix_bot_api.matrix_bot_api import MatrixBotAPI
from matrix_bot_api.mregex_handler import MRegexHandler
from matrix_bot_api.mcommand_handler import MCommandHandler

# Global variables
from config import *

def get_garfeed():
    contents = str(urllib.request.urlopen("https://www.bgreco.net/garfield/").read())
    # funny way of parsing this html
    url = "https://www.bgreco.net/garfield/save.png" + contents.split("save.png")[1].split(">")[0]
    with open("save.png", "wb") as file:
        file.write(urllib.request.urlopen(url).read())
    return url


def sendmsgs(bot):
    get_garfeed()
    url = None
    with open("save.png", "rb") as file:
        url = bot.client.upload(file, "image/png")
    if url is not None:
        for room in bot.rooms:
                room.send_image(url, "garfeeld.png")

def main():
    bot = MatrixBotAPI(USERNAME, PASSWORD, SERVER)
    bot.start_polling()
    bot.client.api.upload_keys()

    print(f"up and running as {USERNAME}")

    while True:
        sendmsgs(bot)

        timenext = random.randint(60, 60*60)
        print(f"next in {timenext} seconds")
        time.sleep(timenext)


if __name__ == "__main__":
    main()
